import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscarApariencia'
})
export class BuscarAparienciaPipe implements PipeTransform {

  transform(value2: any, arg2: any ): any {
    const buscarporApariencia = [];
    for ( const recorrido of value2) {
      if ( recorrido.biography.alignment.toLowerCase().indexOf(arg2.toLowerCase()) > -1) {
        buscarporApariencia.push(recorrido);
      }
    }
    return buscarporApariencia;
  }
}
